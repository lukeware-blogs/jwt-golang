package dto

import "time"

type TokenResponse struct {
	Name    string
	Token   string
	Expires time.Time
}
