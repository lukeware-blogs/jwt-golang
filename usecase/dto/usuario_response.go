package dto

type UsuarioResponse struct {
	Apelido                string
	Senha                  string
	DocumentoIdentificacao string
}
