package dto

type UsuarioRequest struct {
	Apelido string `json:"apelido"`
	Senha   string `json:"senha"`
}
