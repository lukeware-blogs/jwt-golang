package dto

import (
	"time"
)

type JwtResponse struct {
	Apelido                string
	DocumentoIdentificacao string
	ExpirationTime         time.Time
	Token                  string
}
