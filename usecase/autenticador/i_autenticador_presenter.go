package autenticador

import (
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
)

type IAutenticadorPresenter interface {
	Sucesso(response dto.JwtResponse) (*dto.TokenResponse, error)
}
