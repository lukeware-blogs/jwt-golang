package autenticador

import (
	"github.com/stretchr/testify/assert"
	geradorjwt2 "labsit.io/lukeware-blog/jwt-golang/framework/token"
	"labsit.io/lukeware-blog/jwt-golang/interface_adapters/gateway"
	"labsit.io/lukeware-blog/jwt-golang/interface_adapters/presenter"
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
	"strings"
	"testing"
)

func Test_autenticar_com_usuario_valido(t *testing.T) {
	var usuarioRequest = dto.UsuarioRequest{
		Apelido: "admin",
		Senha:   "admin123",
	}

	geradorJWT := geradorjwt2.CreateGeradorJwt()
	autenticadorPresenter := presenter.CreateAutenticadorPresenter()
	autenticadorGateway := gateway.CreateUsuarioGateway()
	autenticadorUsecase := CreatAutenticadorUseCase(autenticadorGateway, autenticadorPresenter, geradorJWT)

	jwtResponse, err := autenticadorUsecase.Entrar(usuarioRequest)

	assert.Equal(t, jwtResponse.Name, "token")
	assert.NotNil(t, jwtResponse.Token)
	assert.True(t, strings.Contains(jwtResponse.Token, "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9"))
	assert.NotNil(t, jwtResponse.Expires)
	assert.Nil(t, err)
}

func Test_autenticar_com_senha_invalida(t *testing.T) {
	var usuarioRequest = dto.UsuarioRequest{
		Apelido: "admin",
		Senha:   "senha_errada",
	}

	geradorJWT := geradorjwt2.CreateGeradorJwt()
	autenticadorPresenter := presenter.CreateAutenticadorPresenter()
	autenticadorGateway := gateway.CreateUsuarioGateway()
	autenticadorUsecase := CreatAutenticadorUseCase(autenticadorGateway, autenticadorPresenter, geradorJWT)

	jwtResponse, err := autenticadorUsecase.Entrar(usuarioRequest)

	assert.Nil(t, jwtResponse)
	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "Senha informada, está inválida")
}
