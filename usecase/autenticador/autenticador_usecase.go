package autenticador

import (
	"labsit.io/lukeware-blog/jwt-golang/entities"
	"labsit.io/lukeware-blog/jwt-golang/framework/token"
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
)

type AutenticadorUseCase struct {
	usuarioGateway        IUsuarioGateway
	autenticadorPresenter IAutenticadorPresenter
	geradorJwt            token.IGeradorJwt
}

func (a AutenticadorUseCase) Entrar(usuarioRequest dto.UsuarioRequest) (*dto.TokenResponse, error) {
	usuarioResponse, _ := a.usuarioGateway.FindOne(usuarioRequest.Apelido)

	usuario := entities.Usuario{
		Apelido:                usuarioResponse.Apelido,
		DocumentoIdentificacao: usuarioResponse.DocumentoIdentificacao,
		Senha:                  usuarioResponse.Senha,
	}

	senhaInvalida := usuario.SenhaEstaValida(usuarioRequest.Senha)

	if senhaInvalida != nil {
		return nil, senhaInvalida
	}

	jwtResponse, _ := a.geradorJwt.GerarJwt(*usuarioResponse)
	return a.autenticadorPresenter.Sucesso(*jwtResponse)
}

func CreatAutenticadorUseCase(autenticadorGateway IUsuarioGateway, autenticadorPresenter IAutenticadorPresenter, geradorJwt token.IGeradorJwt) AutenticadorUseCase {
	return AutenticadorUseCase{
		usuarioGateway:        autenticadorGateway,
		autenticadorPresenter: autenticadorPresenter,
		geradorJwt:            geradorJwt,
	}
}
