package autenticador

import (
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
)

type IAutenticadorUseCase interface {
	Entrar(usuarioRequest dto.UsuarioRequest) (*dto.TokenResponse, error)
}
