package autenticador

import (
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
)

type IUsuarioGateway interface {
	FindOne(apelido string) (*dto.UsuarioResponse, error)
}
