package main

import "labsit.io/lukeware-blog/jwt-golang/framework"

func main() {
	new(framework.Application).Init()
}
