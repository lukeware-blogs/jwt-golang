package controller

import (
	"labsit.io/lukeware-blog/jwt-golang/usecase/autenticador"
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
)

type AutenticadorController struct {
	autentucadorUseCase autenticador.IAutenticadorUseCase
}

func (a AutenticadorController) Autenticar(request dto.UsuarioRequest) (*dto.TokenResponse, error) {
	return a.autentucadorUseCase.Entrar(request)
}

func CreateAutenticadorController(autentucadorUseCase autenticador.IAutenticadorUseCase) AutenticadorController {
	return AutenticadorController{autentucadorUseCase: autentucadorUseCase}
}
