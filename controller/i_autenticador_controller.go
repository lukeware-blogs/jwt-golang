package controller

import "labsit.io/lukeware-blog/jwt-golang/usecase/dto"

type IAutenticadorController interface {
	Autenticar(request dto.UsuarioRequest) (*dto.TokenResponse, error)
}
