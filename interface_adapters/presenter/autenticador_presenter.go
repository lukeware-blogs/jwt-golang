package presenter

import (
	"fmt"
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
)

var jwtKey = []byte("my_secret_key")

const NAME_CHAVE = "token"

type AutenticadorPresenter struct {
}

func (a AutenticadorPresenter) Sucesso(response dto.JwtResponse) (*dto.TokenResponse, error) {
	return &dto.TokenResponse{
		Name:    NAME_CHAVE,
		Token:   response.Token,
		Expires: response.ExpirationTime,
	}, nil
}

func (a AutenticadorPresenter) BemVindo(usuario string) *dto.Response {
	return &dto.Response{
		Usuario:  usuario,
		Mensagem: fmt.Sprintf("Bem vindo %s!", usuario),
	}
}

func CreateAutenticadorPresenter() AutenticadorPresenter {
	return AutenticadorPresenter{}
}
