package gateway

import (
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
)

type UsuarioGateway struct {
}

func (a UsuarioGateway) FindOne(apelido string) (*dto.UsuarioResponse, error) {
	return &dto.UsuarioResponse{
		Apelido:                "admin",
		DocumentoIdentificacao: "999.999.999-99",
		Senha:                  "admin123",
	}, nil
}

func CreateUsuarioGateway() UsuarioGateway {
	return UsuarioGateway{}
}
