package api

import (
	"net/http"
)

type IRecepcionadorRestApi interface {
	BemVindo(w http.ResponseWriter, r *http.Request)
}
