package api

import (
	"encoding/json"
	"labsit.io/lukeware-blog/jwt-golang/controller"
	"labsit.io/lukeware-blog/jwt-golang/framework/token"
	"labsit.io/lukeware-blog/jwt-golang/interface_adapters/gateway"
	"labsit.io/lukeware-blog/jwt-golang/interface_adapters/presenter"
	"labsit.io/lukeware-blog/jwt-golang/usecase/autenticador"
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
	"log"
	"net/http"
)

type AutenticadorRestApi struct {
	autenticadorController controller.IAutenticadorController
	atualizadorToken       token.AtualizadorToken
}

func (a AutenticadorRestApi) Atualizar(w http.ResponseWriter, r *http.Request) {

	if r.Method != PUT {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	tokenResponse, errAutenticacao := a.atualizadorToken.Atualizar(r)

	if errAutenticacao != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if tokenResponse == nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:     tokenResponse.Name,
		Value:    tokenResponse.Token,
		Expires:  tokenResponse.Expires,
		HttpOnly: true,
	})
}

func (a AutenticadorRestApi) Autenticar(w http.ResponseWriter, r *http.Request) {
	if r.Method != POST {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	var usuarioRequest dto.UsuarioRequest
	err := json.NewDecoder(r.Body).Decode(&usuarioRequest)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	tokenResponse, errAutenticacao := a.autenticadorController.Autenticar(usuarioRequest)

	if errAutenticacao != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:     tokenResponse.Name,
		Value:    tokenResponse.Token,
		Expires:  tokenResponse.Expires,
		HttpOnly: true,
	})
}

func CreateAutenticadorRest() IAutenticadorRestApi {
	log.Println("Iniciando o serviço de autenticação")
	geradorJWT := token.CreateGeradorJwt()
	autenticadorGateway := gateway.CreateUsuarioGateway()
	autenticadorPresenter := presenter.CreateAutenticadorPresenter()
	autenticadorUsecase := autenticador.CreatAutenticadorUseCase(autenticadorGateway, autenticadorPresenter, geradorJWT)
	controllerAutenticador := controller.CreateAutenticadorController(autenticadorUsecase)
	atualizadorToken := token.CreateAtualizadorToken()
	return AutenticadorRestApi{autenticadorController: controllerAutenticador, atualizadorToken: atualizadorToken}
}
