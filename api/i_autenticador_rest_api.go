package api

import (
	"net/http"
)

type IAutenticadorRestApi interface {
	Autenticar(w http.ResponseWriter, r *http.Request)
	Atualizar(w http.ResponseWriter, r *http.Request)
}
