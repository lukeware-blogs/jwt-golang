package api

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"labsit.io/lukeware-blog/jwt-golang/framework/token"
	"labsit.io/lukeware-blog/jwt-golang/interface_adapters/presenter"
	"log"
	"net/http"
)

type RecepcionadorRestApi struct {
	recepcionadorPresenter presenter.AutenticadorPresenter
	validadorToken         token.IValidadorToken
}

func (a RecepcionadorRestApi) BemVindo(w http.ResponseWriter, r *http.Request) {
	if r.Method != GET {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	usuarioAutenticado, errToken := a.validadorToken.Validar(r)
	if errToken != nil {
		if errToken == jwt.ErrSignatureInvalid {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		return

	}
	if usuarioAutenticado == nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	response := a.recepcionadorPresenter.BemVindo(usuarioAutenticado.Apelido)

	jsonData, _ := json.Marshal(response)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
	w.WriteHeader(http.StatusOK)
}

func CreateRecepcionadorRest() RecepcionadorRestApi {
	log.Println("Iniciando o serviço de recpcionamento do usuário logado")
	recepcionadorPresenter := presenter.CreateAutenticadorPresenter()
	validadorToken := token.CreateValidadorToken()
	return RecepcionadorRestApi{recepcionadorPresenter: recepcionadorPresenter, validadorToken: validadorToken}
}
