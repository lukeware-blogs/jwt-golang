# jwt-golang

## Serviços criado

### Autenticação

```shell
curl --location --request POST 'http://localhost:8000/signin' \
--header 'Content-Type: application/json' \
--data-raw '{
    "apelido":"admin",
    "senha": "admin123"
}'
```

### Boa vindas ao usuário autenticado

```shell
curl --location --request GET 'http://localhost:8000/welcome' \
--header 'Cookie: token=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJBcGVsaWRvIjoiYWRtaW4iLCJEb2N1bWVudG9JZGVudGlmaWNhY2FvIjoiOTk5Ljk5OS45OTktOTkiLCJFeHBpcmF0aW9uVGltZSI6IjIwMjItMDYtMDdUMTA6NTY6MzcuMDMwMTkyNS0wMzowMCIsImV4cCI6MTY1NDYxMDE5N30.ftfydyO1GfV194RdExjVawqcxrS9hBHQQ6R7RC9c0g67SMHNT1O7k45oWSWEqJ458xJAem76dXCljmu7VH6FhQ'
```

### Atualização do token

```shell
curl --location --request PUT 'http://localhost:8000/refresh' \
--header 'Cookie: token=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJBcGVsaWRvIjoiYWRtaW4iLCJEb2N1bWVudG9JZGVudGlmaWNhY2FvIjoiOTk5Ljk5OS45OTktOTkiLCJFeHBpcmF0aW9uVGltZSI6IjIwMjItMDYtMDdUMTE6MjA6MDMuMDUxMTM0OS0wMzowMCIsImV4cCI6MTY1NDYxMTYwM30.CuaK-2yAFkDGLlJmQGzLG_RKSv1yuGT8QJY8E77DyWpc-85xC29IlQvB2u327k9SWvWJ-MwXdqEAOBJjqRkANg'
```

