package entities

import "fmt"

type Usuario struct {
	Apelido                string
	DocumentoIdentificacao string
	Senha                  string
}

func (u Usuario) SenhaEstaValida(senhaInformada string) error {
	if u.Senha != senhaInformada {
		return fmt.Errorf("Senha informada, está inválida")
	}
	return nil
}
