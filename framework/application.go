package framework

import (
	"labsit.io/lukeware-blog/jwt-golang/api"
	"log"
	"net/http"
)

type Application struct {
}

func (a Application) Init() {
	log.Println("Iniciando aplicação")
	var autenticador api.IAutenticadorRestApi = api.CreateAutenticadorRest()
	var recepcionador api.RecepcionadorRestApi = api.CreateRecepcionadorRest()

	http.HandleFunc("/signin", autenticador.Autenticar)
	http.HandleFunc("/welcome", recepcionador.BemVindo)
	http.HandleFunc("/refresh", autenticador.Atualizar)

	log.Println("Aplicação iniciada com sucesso!")
	log.Fatal(http.ListenAndServe(":8000", nil))

}
