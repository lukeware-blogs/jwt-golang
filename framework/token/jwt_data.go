package token

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

type JwtData struct {
	Apelido                string
	DocumentoIdentificacao string
	ExpirationTime         time.Time
	jwt.StandardClaims
}
