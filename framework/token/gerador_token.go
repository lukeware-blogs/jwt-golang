package token

import (
	"github.com/dgrijalva/jwt-go"
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
	"time"
)

type GeradorToken struct{}

const duration = 5 * time.Minute

var JWT_KEY = []byte("my_secret_key")

func (g GeradorToken) GerarJwt(usuarioResponse dto.UsuarioResponse) (*dto.JwtResponse, error) {
	expirationTime := time.Now().Add(duration)

	jwtData := JwtData{
		Apelido:                usuarioResponse.Apelido,
		DocumentoIdentificacao: usuarioResponse.DocumentoIdentificacao,
		ExpirationTime:         expirationTime,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS512, jwtData)

	tokenString, err := token.SignedString(JWT_KEY)
	if err != nil {
		return nil, err
	}

	return &dto.JwtResponse{
		Apelido:                usuarioResponse.Apelido,
		DocumentoIdentificacao: usuarioResponse.DocumentoIdentificacao,
		ExpirationTime:         expirationTime,
		Token:                  tokenString,
	}, nil
}

func CreateGeradorJwt() IGeradorJwt {
	return GeradorToken{}
}
