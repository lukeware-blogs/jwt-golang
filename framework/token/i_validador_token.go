package token

import (
	"net/http"
)

type IValidadorToken interface {
	Validar(r *http.Request) (*JwtData, error)
}
