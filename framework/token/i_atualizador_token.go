package token

import (
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
	"net/http"
)

type IAtualizadorToken interface {
	Atualizar(r *http.Request) (*dto.TokenResponse, error)
}
