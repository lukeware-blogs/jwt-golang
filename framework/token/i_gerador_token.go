package token

import (
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
)

type IGeradorJwt interface {
	GerarJwt(usuarioResponse dto.UsuarioResponse) (*dto.JwtResponse, error)
}
