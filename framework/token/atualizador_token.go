package token

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"labsit.io/lukeware-blog/jwt-golang/usecase/dto"
	"net/http"
	"time"
)

type AtualizadorToken struct {
}

const TOKEN_KEY = "token"

func (a AtualizadorToken) Atualizar(r *http.Request) (*dto.TokenResponse, error) {
	c, err := r.Cookie(TOKEN_KEY)
	if err != nil {
		return nil, err
	}

	tknStr := c.Value
	jwtData := &JwtData{}
	tkn, err := jwt.ParseWithClaims(tknStr, jwtData, func(token *jwt.Token) (interface{}, error) {
		return JWT_KEY, nil
	})

	if err != nil {
		return nil, err
	}

	if !tkn.Valid {
		return nil, nil
	}

	tempoRestanteToken := time.Unix(jwtData.ExpiresAt, 0).Sub(time.Now())
	tempoLimitePosTokenExpirado := 2 * time.Minute
	if tempoRestanteToken > tempoLimitePosTokenExpirado {
		fmt.Printf("Tempo restante do token: %v\t-\tTempo limite pós expirar: %v\n", tempoRestanteToken, tempoLimitePosTokenExpirado)
		return nil, nil
	}

	expirationTime := time.Now().Add(duration)
	jwtData.ExpiresAt = expirationTime.Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwtData)
	tokenString, err := token.SignedString(JWT_KEY)
	if err != nil {
		return nil, err
	}

	return &dto.TokenResponse{
		Name:    TOKEN_KEY,
		Token:   tokenString,
		Expires: expirationTime,
	}, nil
}

func CreateAtualizadorToken() AtualizadorToken {
	return AtualizadorToken{}
}
