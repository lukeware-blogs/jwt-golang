package token

import (
	"github.com/dgrijalva/jwt-go"
	"net/http"
)

type ValidadorToken struct {
}

func (v ValidadorToken) Validar(r *http.Request) (*JwtData, error) {
	c, err := r.Cookie("token")

	if err != nil {
		return nil, err
	}
	jwtData := &JwtData{}
	tkn, err := jwt.ParseWithClaims(c.Value, jwtData, func(token *jwt.Token) (interface{}, error) {
		return JWT_KEY, nil
	})

	if err != nil {
		return nil, err
	}

	if !tkn.Valid {
		return nil, nil
	}

	return jwtData, nil
}

func CreateValidadorToken() ValidadorToken {
	return ValidadorToken{}
}
